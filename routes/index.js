var express = require('express');
var router = express.Router();
var Dew = require('../models/dews')

//#region Get
/* GET home page. */
router.get('/', function (req, res, next) {
    res.status(200).send({
        "welcomeMessage": "Congrats you\'ve reached the Honeydew API",
        "status": 200
    })
});

router.get('/listOfDews', (req, res) => {
    Dew.getDews(req, res, (err, dews) => {
        console.log(('outgoing Dews',dews));
        
        res.json(dews)
    })

    // res.send({
    //     dews: res.locals.dews
    // })
})
//#endregion

//#region Post
router.post('/addDew', (req, res, next) => {
    Dew.newDew(req, res, (err) => {
        if (err) {
            res.status(404).send(err)
        } else {
            res.sendStatus(200)
        }
    })
})

router.post('/completeDew', (req, res, next) => {
    Dew.completeDew(req, res, (err, status) => {
        if (err) {
            res.json(status)
        } else {
            res.json(status)
        }
    })
})
//#endregion

module.exports = router;