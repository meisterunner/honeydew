
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose')

var indexRouter = require('./routes/index');

var app = express();


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(cookieParser());

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = err

    // render the error page
    res.status(err.status || 500).send({
        error: err,
        errorMessage: err.message
    })
});

mongoose.connect('mongodb://192.168.0.7/honeydew', {
    useNewUrlParser: true
})
mongoose.connection.once('open', ()=>{
    console.log('Connected to Mongo');
    
})

module.exports = app;