//Build dews Schema

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Dew = new Schema({
    assignedTo: String,
    assignedFrom: String,
    assignment: String,
    dateAssigned: {
        type: Date,
        default: Date.now
    },
    dateCompleted: Date,
    completedBy: String,
    completed: {
        type: Boolean,
        default: false
    }
})

module.exports = Dew = mongoose.model('Dew', Dew)

//Methods
/**
 * Get a List of all non-completed Dews
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
module.exports.getDews = function (req, res, next) {
    Dew.find({
        completed: false
    }, next)
}
/**
 * Add a new Gew
 * @param {*} res 
 * @param {*} req 
 * @param {*} next 
 */
module.exports.newDew = (req, res, next) => {
      let newDew = new Dew({
        assignedTo: req.body.assignedTo,
        assignedFrom: req.body.assignedFrom,
        assignment: req.body.assignment
    })
    newDew.save((err) => {
        if (err) {
            console.log(err);
        }
        next(err);
    })
}
/**
 * Finds a Dew by it's Id then updates completed and dateCompleted
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
module.exports.completeDew = (req, res, next) => {
    Dew.findById(req.body.id, (err, result) => {
        if (err) {
            res.locals.success = false;
            next(err, res.locals.success)
        } else {
            saveDew(result);
        }
    })

    function saveDew(result) {
        if (typeof result !== "undefined") {
            result.completed = true;
            result.dateCompleted = new Date();
            result.save((err) => {
                if (err) {
                    console.log(err);
                    res.locals.success = false;
                } else {
                    res.locals.success = true;
                }
                next(err, res.locals.success)
            })
        } else {
            res.locals.success = false;
            next(err, res.locals.success)
        }
    }
}