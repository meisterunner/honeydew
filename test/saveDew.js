const conn = require('./connect');
const assert = require('assert');
const Dew = require('../models/dews');

describe('Save Dew', () => {
    assignedTo = 'John'
    assignedFrom = 'Monika'
    assignment = "Take out the garbage."
    var dew;

    beforeEach((done) => {
        dew = new Dew({
            assignedTo,
            assignedFrom,
            assignment
        })
        done()
    })
    it('Saves a New Dew and returns a non empty _id value', (done) => {
        dew.save((err, dew) => {
            if (!err) {
                assert(dew._id.toString() != '')
                done();
            }
        })
    })
    it('Saves a New Dew and checks that default dateAssigned was set', (done) => {
        dew.save((err, aDew) => {
            if (!err) {
                assert(aDew.dateAssigned.toString() != '')
                done();
            }
        })
    })
    it('Saves a New Dew and checks that default completed was set to false', (done) => {
        dew.save((err, aDew) => {
            if (!err) {
                assert(aDew.completed == false)
                done();
            }
        })
    })
    it('Saves a New Dew and checks that assignedTo =John', (done) => {
        dew.save((err, aDew) => {
            if (!err) {
                assert(aDew.assignedTo === 'John')
                done();
            }
        })
    })
    it('Saves a New Dew and tests to fail on assignedTo not = John', (done) => {
        dew.save((err, aDew) => {
            if (!err) {
                assert(aDew.assignedTo != 'John')
                done();
            }
        })
    })
})