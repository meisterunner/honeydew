var mongoose = require('mongoose');

beforeEach((next) => {
    mongoose.connect('mongodb://localhost/test', {
        useNewUrlParser: true
    })
    mongoose.connection.once('open', () => {
        next();
    })
})

beforeEach((next) => {
    mongoose.connection.collections.dews.drop(() => {
        next();
    })
})
